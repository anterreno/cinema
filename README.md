## Getting started

- npm install
- npm start

## Resumen

Aplicación de películas con un registro e inicio de sesión de usuarios con email y contraseña utilizando firebase authentication. La misma cuenta con un listado de películas desde el cual se puede ver el detalle, editar y eliminar cada una de ellas. También, cuenta con la opción de agregar nuevas películas. 
Por otra parte, desde el menú, tiene la opción de cambiar tu logo entrando a la opción perfil. 
Finalmente, se puede cerrar la sesión iniciada. 

## Consideraciones

- Las películas no son persistentes, es decir, al refrescar la app, la data modificada volverá a estar como al principio.
- Las imágenes de las peliculas a agregar deben ser seleccionadas desde la carpeta propia del proyecto `assets/img/movies` para que se muestren correctamente. Esto se debe a que los datos se encuentran de manera local. 
- Los logos del perfil deben ser seleccionados desde la carpeta propia del proyecto `assets/img/logos` para que se muestren correctamente. Esto se debe que los datos se encuentras de manera local.

## Teconologías utilizadas

- Angular 
- Ionic
- Firebase Authentication
- SASS
- Jasmine & karma (unit tests) - WiP
