import { Injectable } from '@angular/core';
import { UrlTree } from '@angular/router';
import { NavController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { UsersService } from 'src/app/modules/users/shared-users/service/users.service';

@Injectable({
  providedIn: 'root',
})
export class AuthGuard {
  constructor(
    private UsersService: UsersService,
    private navController: NavController
  ) {}

  canActivate():
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {
    if (this.UsersService.isLogged) {
      return true;
    }
    this.goToLogin();
    return this.UsersService.isLogged;
  }

  goToLogin() {
    this.navController.navigateRoot('users/login');
  }
}
