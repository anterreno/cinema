import { NgModule, OnInit } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy, RouterModule } from '@angular/router';
import { AngularFireModule } from '@angular/fire/compat';
import { IonicRouteStrategy } from '@ionic/angular';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { UsersModule } from './modules/users/users.module';
import { environment } from 'src/environments/environment';
import { MoviesHomePageModule } from './modules/movies/movies-home/movies-home.module';
import { SharedModule } from './shared/shared.module';
import { EditMoviePageModule } from './modules/movies/edit-movie/edit-movie.module';
import { MovieDetailPageModule } from './modules/movies/movie-detail/movie-detail.module';
import { LogoPageModule } from './modules/users/logo/logo.module';
import { MoviesModule } from './modules/movies/movies.module';
import { IonicStorageModule, Storage } from '@ionic/storage-angular';
import { AddMoviePageModule } from './modules/movies/add-movie/add-movie.module';
import { VerificationEmailPageModule } from './modules/users/verification-email/verification-email.module';
import { ForgotPasswordPageModule } from './modules/users/forgot-password/forgot-password.module';

@NgModule({
  declarations: [AppComponent],
  imports: [
    SharedModule,
    BrowserModule,
    AppRoutingModule,
    RouterModule,
    IonicStorageModule.forRoot(),
    AngularFireModule.initializeApp(environment.firebase),
    MoviesModule,
    UsersModule,
    MoviesHomePageModule,
    EditMoviePageModule,
    MovieDetailPageModule,
    LogoPageModule,
    AddMoviePageModule,
    VerificationEmailPageModule,
    ForgotPasswordPageModule,
  ],
  providers: [{ provide: RouteReuseStrategy, useClass: IonicRouteStrategy }],
  bootstrap: [AppComponent],
})
export class AppModule implements OnInit {
  constructor(private storage: Storage) {}

  ngOnInit(): void {
    this.storage.create();
  }
}
