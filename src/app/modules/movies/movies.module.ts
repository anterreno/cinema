import { NgModule } from '@angular/core';
import { MoviesRoutingModule } from './movies-routing.module';

@NgModule({
  declarations: [],
  imports: [MoviesRoutingModule],
})
export class MoviesModule {}
