import { NgModule } from '@angular/core';
import { MovieDetailPage } from './movie-detail.page';
import { RouterModule, Routes } from '@angular/router';
import { SharedMoviesModule } from '../shared/shared-movies.module';
import { IonRatingStarsModule } from 'ion-rating-stars';

const routes: Routes = [
  {
    path: '',
    component: MovieDetailPage,
  },
  {
    path: ':id',
    component: MovieDetailPage,
  },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    SharedMoviesModule,
    IonRatingStarsModule,
  ],
  exports: [RouterModule],
  declarations: [MovieDetailPage],
})
export class MovieDetailPageModule {}
