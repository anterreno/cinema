import { Component, OnInit } from '@angular/core';
import { ModalController, NavController } from '@ionic/angular';
import { MoviesService } from '../shared/services/movies/movies.service';
import { ActivatedRoute } from '@angular/router';
import { ModalComponent } from '../shared/components/modal/modal.component';

@Component({
  selector: 'app-movie-detail',
  template: ` <ion-header mode="md">
      <ion-toolbar color="black">
        <ion-buttons class="back" slot="start">
          <ion-back-button
            text=""
            mode="ios"
            defaultHref=""
            (click)="backButton()"
          ></ion-back-button>
        </ion-buttons>
        <ion-buttons class="actions" slot="end">
          <ion-icon
            class="actions__delete"
            (click)="this.showDeleteAlert()"
            name="trash-sharp"
          ></ion-icon>
          <ion-icon
            class="actions__edit"
            (click)="this.goToEdit()"
            name="pencil-sharp"
          ></ion-icon>
        </ion-buttons>
      </ion-toolbar>
    </ion-header>
    <ion-content class="md" [scrollY]="true">
      <div class="md__title">
        <ion-text>{{ this.movie.title }}</ion-text>
      </div>
      <div class="md__content">
        <div class="md__content__img">
          <img [src]="this.movie.img" alt="" />
        </div>
        <div class="md__content__description">
          <ion-text>
            {{ this.movie.description }}
          </ion-text>
        </div>
        <div class="md__content__rate">
          <div class="md__content__rate__title">
            <ion-text>Rate</ion-text>
          </div>
          <ion-rating-stars
            [rating]="this.movie.rate"
            [color]="'darkgrey'"
            [filledColor]="'orange'"
            [margin]="10"
            [size]="25"
            [disabled]="true"
          ></ion-rating-stars>
        </div>
      </div>
    </ion-content>`,
  styleUrls: ['./movie-detail.page.scss'],
})
export class MovieDetailPage implements OnInit {
  movie: any;
  movieFiltered: any;
  id: any;

  constructor(
    private navController: NavController,
    private moviesService: MoviesService,
    private route: ActivatedRoute,
    private modalController: ModalController
  ) {}

  ngOnInit() {
    this.id = this.route.snapshot.paramMap.get('id');
    this.getMovie();
  }

  getMovie() {
    this.movie = this.moviesService.getMovie(parseInt(this.id));
  }

  async showDeleteAlert() {
    const modal = await this.modalController.create({
      component: ModalComponent,
      cssClass: 'modal',
      backdropDismiss: false,
      componentProps: {
        description:
          'You are about to delete a movie. Are you sure you want to do it?',
        confirmButton: 'Yes',
        cancelButton: 'No',
        color: 'danger',
      },
    });

    await modal.present();
    const { role } = await modal.onDidDismiss();
    if (role === 'confirm') {
      await this.deleteMovie();
    }
  }

  deleteMovie() {
    this.moviesService.deleteMovie(this.movie.id);
    this.navController.navigateBack('/movies/home');
  }

  goToEdit() {
    this.navController.navigateForward('movies/edit/' + this.movie.id);
  }

  backButton() {
    this.navController.navigateBack('movies/home');
  }
}
