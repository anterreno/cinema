import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import {
  UntypedFormBuilder,
  UntypedFormGroup,
  Validators,
} from '@angular/forms';
import { MoviesService } from '../shared/services/movies/movies.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-edit-movie',
  template: ` <ion-header mode="md">
      <ion-toolbar color="black">
        <ion-buttons slot="start">
          <ion-back-button
            text=""
            mode="ios"
            defaultHref=""
            (click)="backButton()"
          ></ion-back-button>
        </ion-buttons>
        <ion-title>Edit movie</ion-title>
      </ion-toolbar>
    </ion-header>
    <ion-content class="em" [scrollY]="true">
      <div class="em__form">
        <div class="em__form__img">
          <img [src]="this.movie.img" alt="" />
        </div>
        <form [formGroup]="this.form">
          <div class="em__form__title">
            <app-custom-input
              [controlName]="'title'"
              [placeholder]="'Title'"
            ></app-custom-input>
          </div>
          <div class="em__form__description"></div>
          <app-custom-input
            [controlName]="'description'"
            [placeholder]="'Description'"
          ></app-custom-input>
          <div class="em__form__rate">
            <div class="em__form__rate__title">
              <ion-text>Rate:</ion-text>
            </div>
            <div class="em__form__rate_stars">
              <ion-rating-stars
                [rating]="this.movie.rate"
                [color]="'darkgrey'"
                [filledColor]="'orange'"
                [margin]="2"
                [size]="20"
                [disabled]="false"
                (ratingChange)="this.change($event)"
              ></ion-rating-stars>
            </div>
          </div>
        </form>
      </div>
    </ion-content>
    <ion-footer class="footer">
      <div class="em__button">
        <app-custom-button
          (buttonClick)="this.saveData()"
          [disabled]="!this.form.valid"
          [buttonName]="'Save'"
        ></app-custom-button>
      </div>
    </ion-footer>`,
  styleUrls: ['./edit-movie.page.scss'],
})
export class EditMoviePage implements OnInit {
  form: UntypedFormGroup = this.formBuilder.group({
    title: ['', [Validators.required]],
    description: ['', [Validators.required]],
    rate: ['', [Validators.required]],
  });
  movie: any;
  movieId: any;
  movieFiltered: any;
  constructor(
    private navController: NavController,
    private formBuilder: UntypedFormBuilder,
    private moviesService: MoviesService,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.movieId = this.route.snapshot.paramMap.get('id');
    this.getMovie();
    this.patchValues();
  }

  getMovie() {
    this.movie = this.moviesService.getMovie(parseInt(this.movieId));
  }

  change(rate: number) {
    this.form.patchValue({ rate: rate });
  }

  saveData() {
    const editedMovie = Object.assign(this.form.value, {
      img: this.movie.img,
      id: this.movie.id,
    });
    this.moviesService.editMovie(editedMovie);
    this.navController.navigateBack('movies/home');
  }

  patchValues() {
    this.form.patchValue({
      title: this.movie.title,
      description: this.movie.description,
      rate: this.movie.rate,
    });
  }

  backButton() {
    this.navController.navigateBack('movies/home');
  }
}
