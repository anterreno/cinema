import { NgModule } from '@angular/core';
import { EditMoviePage } from './edit-movie.page';
import { RouterModule, Routes } from '@angular/router';
import { SharedMoviesModule } from '../shared/shared-movies.module';
import { IonRatingStarsModule } from 'ion-rating-stars';

const routes: Routes = [
  {
    path: '',
    component: EditMoviePage,
  },
  {
    path: ':id',
    component: EditMoviePage,
  },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    SharedMoviesModule,
    IonRatingStarsModule,
  ],
  exports: [RouterModule],
  declarations: [EditMoviePage],
})
export class EditMoviePageModule {}
