import { NgModule } from '@angular/core';
import { MoviesHomePage } from './movies-home.page';
import { RouterModule, Routes } from '@angular/router';
import { SharedMoviesModule } from '../shared/shared-movies.module';

const routes: Routes = [
  {
    path: '',
    component: MoviesHomePage,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes), SharedMoviesModule],
  exports: [RouterModule],
  declarations: [MoviesHomePage],
})
export class MoviesHomePageModule {}
