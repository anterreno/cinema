import { Component } from '@angular/core';
import { MoviesService } from '../shared/services/movies/movies.service';
import { NavController } from '@ionic/angular';
import { Movie } from 'src/app/shared/interfaces/movie.interface';

@Component({
  selector: 'app-movies-home',
  template: `<ion-header>
      <ion-toolbar color="black">
        <ion-buttons slot="start">
          <ion-menu-button></ion-menu-button>
        </ion-buttons>
        <ion-title>Movies</ion-title>
      </ion-toolbar>
    </ion-header>
    <ion-content class="mh" [scrollY]="true">
      <ion-searchbar
        color="light"
        animated="true"
        placeholder="Search"
        mode="ios"
        (ionChange)="handleChange($event)"
      ></ion-searchbar>
      <ion-fab
        class="mg__fab"
        vertical="bottom"
        horizontal="center"
        slot="fixed"
        (click)="this.addMovie()"
      >
        <ion-fab-button class="mh__fab__button">
          <ion-icon name="add"></ion-icon>
        </ion-fab-button>
      </ion-fab>
      <app-movie
        [movies]="this.movies"
        (editMovie)="this.edit($event)"
        (movieDetail)="this.detail($event)"
      ></app-movie>
    </ion-content>`,
  styleUrls: ['./movies-home.page.scss'],
})
export class MoviesHomePage {
  movies: Movie[];
  constructor(
    private moviesService: MoviesService,
    private navController: NavController
  ) {}

  ionViewWillEnter() {
    this.getMovies();
  }

  getMovies() {
    this.movies = this.moviesService.getMovies();
  }

  edit(movie: Movie) {
    this.navController.navigateForward('movies/edit/' + movie.id);
  }

  detail(movie: Movie) {
    this.navController.navigateForward('movies/detail/' + movie.id);
  }

  addMovie() {
    this.navController.navigateForward('movies/add');
  }

  handleChange(event: any) {
    const search = event.target.value.toLowerCase();
    if (search) {
      this.movies = this.movies.filter((movie) => {
        return movie.title.toLowerCase().indexOf(search) > -1;
      });
    } else if (search === '') {
      this.getMovies();
    }
  }
}
