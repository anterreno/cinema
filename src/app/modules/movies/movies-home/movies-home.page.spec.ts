import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { MoviesHomePage } from './movies-home.page';

describe('MoviesHomePage', () => {
  let component: MoviesHomePage;
  let fixture: ComponentFixture<MoviesHomePage>;

  beforeEach(waitForAsync(() => {
    fixture = TestBed.createComponent(MoviesHomePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
