import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { Movie } from 'src/app/shared/interfaces/movie.interface';

@Component({
  selector: 'app-movie',
  template: `
    <ion-item-sliding class="sliding" *ngFor="let movie of movies">
      <ion-item class="item" (click)="this.detail(this.movie)">
        <div class="item__img">
          <img [src]="this.movie.img" alt="" />
        </div>
        <div class="item__movie">
          <div class="item__movie__title">
            <ion-text>{{ this.movie.title }}</ion-text>
          </div>
          <div class="item__movie__description">
            <ion-text>{{ this.movie.description }}</ion-text>
          </div>
          <div class="item__movie__description">
            <ion-text>Rate</ion-text>
            <ion-rating-stars
              [rating]="this.movie.rate"
              [color]="'darkgrey'"
              [filledColor]="'orange'"
              [margin]="2"
              [size]="20"
              [disabled]="true"
            ></ion-rating-stars>
          </div>
        </div>
      </ion-item>
      <ion-item-options class="item-options">
        <ion-item-option
          class="item-options__option"
          color="warning"
          (click)="this.edit(this.movie)"
          >Edit</ion-item-option
        >
      </ion-item-options>
    </ion-item-sliding>
  `,
  styleUrls: ['./movie.component.scss'],
})
export class MovieComponent implements OnInit {
  @Input() movies: Movie[];
  @Output() editMovie: EventEmitter<any> = new EventEmitter<any>();
  @Output() movieDetail: EventEmitter<any> = new EventEmitter<any>();

  constructor() {}

  ngOnInit() {}

  edit(movie: Movie) {
    this.editMovie.emit(movie);
  }

  detail(movie: Movie) {
    this.movieDetail.emit(movie);
  }
}
