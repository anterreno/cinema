import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-modal',
  template: `
    <div class="main__close_button">
      <ion-button
        class="ion-no-padding"
        slot="icon-only"
        fill="clear"
        name="Close"
        (click)="this.close()"
      >
        <ion-icon name="close-outline"></ion-icon>
      </ion-button>
    </div>
    <div class="main__body">
      <div class="main__body__content">
        <ion-label
          color="black"
          class="ion-no-margin main__body__content__description"
        >
          {{ this.description }}
        </ion-label>
      </div>
      <div class="main__actions">
        <ion-button class="cancel" fill="clear" (click)="this.cancelAction()">{{
          this.cancelButton
        }}</ion-button>
        <ion-button
          class="cancel"
          fill="outline"
          [color]="this.color"
          (click)="this.confirmAction()"
          >{{ this.confirmButton }}</ion-button
        >
      </div>
    </div>
  `,
  styleUrls: ['./modal.component.scss'],
})
export class ModalComponent implements OnInit {
  description: string;
  confirmButton: string;
  cancelButton: string;
  color: string;
  constructor(private modalController: ModalController) {}

  ngOnInit() {}

  close() {
    this.modalController.dismiss();
  }

  confirmAction() {
    this.modalController.dismiss(null, 'confirm');
  }

  cancelAction() {
    this.modalController.dismiss(null, 'cancel');
  }
}
