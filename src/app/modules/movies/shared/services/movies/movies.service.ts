import { Injectable } from '@angular/core';
import { Movie } from 'src/app/shared/interfaces/movie.interface';
import { MOVIES } from '../../constants/movies';

@Injectable({
  providedIn: 'root',
})
export class MoviesService {
  movies: Movie[] = MOVIES;
  movie: any;
  constructor() {}

  getMovies(): Movie[] {
    return this.movies;
  }

  getMovie(id: number): Movie | undefined {
    return this.movies.find((movie: Movie) => movie.id === id);
  }

  addMovie(movie: Movie): void {
    movie.id = this.movies.length + 1;
    this.movies.push(movie);
  }

  editMovie(movie: Movie): void {
    const index = this.movies.findIndex((m) => m.id === movie.id);
    this.movies[index] = movie;
  }

  deleteMovie(id: number): void {
    this.movies = this.movies.filter((movie) => movie.id !== id);
  }
}
