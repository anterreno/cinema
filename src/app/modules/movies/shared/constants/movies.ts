export const MOVIES = [
  {
    id: 1,
    title: 'Avatar',
    description:
      'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
    rate: 4,
    img: 'assets/img/movies/avatar.png',
  },
  {
    id: 2,
    title: 'Avengers',
    description:
      'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
    rate: 2,
    img: 'assets/img/movies/avengers.png',
  },
  {
    id: 3,
    title: 'Shrek',
    description:
      'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
    rate: 5,
    img: 'assets/img/movies/shrek.png',
  },
  {
    id: 4,
    title: 'Titanic',
    description:
      'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
    rate: 5,
    img: 'assets/img/movies/titanic.png',
  },
];
