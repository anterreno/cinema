import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from 'src/app/guards/auth/auth.guard';

const routes: Routes = [
  {
    path: 'movies',
    canActivate: [AuthGuard],
    children: [
      {
        path: 'home',
        loadChildren: () =>
          import('./movies-home/movies-home.module').then(
            (m) => m.MoviesHomePageModule
          ),
      },
      {
        path: 'edit',
        loadChildren: () =>
          import('./edit-movie/edit-movie.module').then(
            (m) => m.EditMoviePageModule
          ),
      },
      {
        path: 'detail',
        loadChildren: () =>
          import('./movie-detail/movie-detail.module').then(
            (m) => m.MovieDetailPageModule
          ),
      },
      {
        path: 'add',
        loadChildren: () =>
          import('./add-movie/add-movie.module').then(
            (m) => m.AddMoviePageModule
          ),
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MoviesRoutingModule {}
