import { NgModule } from '@angular/core';
import { AddMoviePage } from './add-movie.page';
import { RouterModule, Routes } from '@angular/router';
import { SharedMoviesModule } from '../shared/shared-movies.module';

const routes: Routes = [
  {
    path: '',
    component: AddMoviePage,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes), SharedMoviesModule],
  exports: [RouterModule],
  declarations: [AddMoviePage],
})
export class AddMoviePageModule {}
