import { Component } from '@angular/core';
import { FormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { MoviesService } from '../shared/services/movies/movies.service';
import {
  ModalController,
  NavController,
  ToastController,
} from '@ionic/angular';
import { ModalComponent } from '../shared/components/modal/modal.component';

@Component({
  selector: 'app-add-movie',
  template: `<ion-header mode="md">
      <ion-toolbar color="black">
        <ion-buttons slot="start">
          <ion-back-button
            text=""
            mode="ios"
            defaultHref=""
            (click)="backButton()"
          ></ion-back-button>
        </ion-buttons>
        <ion-title>Add movie</ion-title>
      </ion-toolbar>
    </ion-header>
    <ion-content class="am">
      <div class="am__content">
        <div class="am__content__title">
          <ion-text>Add your new movie</ion-text>
        </div>
        <form [formGroup]="this.form">
          <div class="am__content__form__title">
            <app-custom-input
              [controlName]="'title'"
              [placeholder]="'Title'"
            ></app-custom-input>
          </div>
          <div class="am__content__form__description">
            <app-custom-input
              [controlName]="'description'"
              [placeholder]="'Description'"
            ></app-custom-input>
          </div>
          <div class="am__content__form__rate">
            <app-custom-input
              [controlName]="'rate'"
              [placeholder]="'Rate (1-5)'"
              [type]="'number'"
            ></app-custom-input>
          </div>
          <div class="am__content__form__upload-img">
            <ion-item>
              <input
                formControlName="img"
                class="content__upload-img__item input"
                type="file"
                accept="image/jpeg,image/png"
                (change)="onFileSelected($event)"
              />
            </ion-item>
          </div>
        </form>
      </div>
    </ion-content>
    <ion-footer class="footer">
      <div class="am__content__button">
        <app-custom-button
          (buttonClick)="this.showAddAlert()"
          [disabled]="!this.form.valid"
          [buttonName]="'Save'"
        ></app-custom-button>
      </div>
    </ion-footer> `,
  styleUrls: ['./add-movie.page.scss'],
})
export class AddMoviePage {
  form: UntypedFormGroup = this.formBuilder.group({
    title: ['', [Validators.required]],
    description: ['', [Validators.required]],
    rate: ['', [Validators.required, Validators.maxLength(1)]],
    img: ['', [Validators.required]],
  });
  imageFullPath: string;
  constructor(
    private formBuilder: FormBuilder,
    private moviesService: MoviesService,
    private navController: NavController,
    private modalController: ModalController,
    private toastController: ToastController
  ) {}

  backButton() {
    this.navController.navigateBack('movies/home');
  }

  onFileSelected(event: any) {
    const file = event.target.files[0];
    if (file) {
      const imagePath = file.name;
      this.imageFullPath = 'assets/img/movies/' + imagePath;
    }
  }

  saveMovie() {
    const imageToSave = Object.assign(this.form.value, {
      img: this.imageFullPath,
    });
    this.moviesService.addMovie(imageToSave);
    this.successToast();
    this.navController.navigateForward('movies/home');
  }

  async showAddAlert() {
    const modal = await this.modalController.create({
      component: ModalComponent,
      cssClass: 'modal',
      backdropDismiss: false,
      componentProps: {
        description: 'Are you sure you want to add this movie?',
        confirmButton: 'Yes',
        cancelButton: 'No',
        color: 'success',
      },
    });

    await modal.present();
    const { role } = await modal.onDidDismiss();
    if (role === 'confirm') {
      this.saveMovie();
    }
  }

  async successToast() {
    const toast = await this.toastController.create({
      message: 'Movie successfully added!',
      animated: true,
      duration: 2300,
      position: 'bottom',
      icon: 'checkmark-circle-outline',
      color: 'success',
    });

    await toast.present();
  }
}
