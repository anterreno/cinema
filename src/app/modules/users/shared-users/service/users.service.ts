import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/compat/auth';

@Injectable({
  providedIn: 'root',
})
export class UsersService {
  isLogged = false;
  constructor(public auth: AngularFireAuth) {}

  async signUp(email: string, password: string) {
    this.sendVerificationEmail();
    return await this.auth.createUserWithEmailAndPassword(email, password);
  }

  async signIn(email: string, password: string) {
    this.isLogged = true;
    return await this.auth.signInWithEmailAndPassword(email, password);
  }

  async signOut() {
    this.isLogged = false;
    await this.auth.signOut().then();
  }

  async sendVerificationEmail() {
    return (await this.auth.currentUser)?.sendEmailVerification();
  }

  async recoveryPassword(email: string) {
    return await this.auth.sendPasswordResetEmail(email);
  }
}
