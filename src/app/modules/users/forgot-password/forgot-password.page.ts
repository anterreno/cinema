import { Component, OnInit } from '@angular/core';
import { FormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { UsersService } from '../shared-users/service/users.service';
import { NavController, ToastController } from '@ionic/angular';

@Component({
  selector: 'app-forgot-password',
  template: `<ion-header>
      <ion-toolbar color="black">
        <ion-buttons slot="start">
          <ion-back-button
            text=""
            mode="ios"
            defaultHref=""
            (click)="backButton()"
          ></ion-back-button>
        </ion-buttons>
        <ion-title>Password recovery</ion-title>
      </ion-toolbar>
    </ion-header>
    <ion-content class="fp">
      <div class="fp__content">
        <form class="fp__content__form" [formGroup]="this.form">
          <div class="fp__content__form__email">
            <div class="fp__content__form__email__label">
              <label>Enter the email with which you registered</label>
            </div>
            <app-custom-input
              [controlName]="'email'"
              [placeholder]="'Email'"
            ></app-custom-input>
          </div>
        </form>
        <div class="fp__content__button">
          <app-custom-button
            (buttonClick)="this.recoveryPassword()"
            [buttonName]="'Request a password reset'"
          ></app-custom-button>
        </div>
      </div>
    </ion-content> `,
  styleUrls: ['./forgot-password.page.scss'],
})
export class ForgotPasswordPage implements OnInit {
  form: UntypedFormGroup = this.formBuilder.group({
    email: ['', [Validators.required, Validators.email]],
  });
  constructor(
    private formBuilder: FormBuilder,
    private usersService: UsersService,
    private navController: NavController,
    private toastController: ToastController
  ) {}

  ngOnInit() {}

  recoveryPassword() {
    this.usersService.recoveryPassword(this.form.value.email);
    this.infoToast();
    this.navController.navigateForward('users/login');
  }

  backButton() {
    this.navController.navigateBack('users/login');
  }

  async infoToast() {
    const toast = await this.toastController.create({
      message: 'Check your inbox or spam!',
      animated: true,
      duration: 2000,
      position: 'bottom',
      icon: 'warning-outline',
      color: 'warning',
    });

    await toast.present();
  }
}
