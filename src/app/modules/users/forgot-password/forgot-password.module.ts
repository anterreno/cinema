import { NgModule } from '@angular/core';
import { ForgotPasswordPage } from './forgot-password.page';
import { RouterModule, Routes } from '@angular/router';
import { SharedUsersModule } from '../shared-users/shared-users.module';

const routes: Routes = [
  {
    path: '',
    component: ForgotPasswordPage,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes), SharedUsersModule],
  exports: [RouterModule],
  declarations: [ForgotPasswordPage],
})
export class ForgotPasswordPageModule {}
