import { NgModule } from '@angular/core';
import { VerificationEmailPage } from './verification-email.page';
import { RouterModule, Routes } from '@angular/router';
import { SharedUsersModule } from '../shared-users/shared-users.module';

const routes: Routes = [
  {
    path: '',
    component: VerificationEmailPage,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes), SharedUsersModule],
  exports: [RouterModule],
  declarations: [VerificationEmailPage],
})
export class VerificationEmailPageModule {}
