import { Component, OnInit } from '@angular/core';
import { NavController, ToastController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { UsersService } from '../shared-users/service/users.service';

@Component({
  selector: 'app-verification-email',
  template: `<ion-header mode="md">
      <ion-toolbar color="black">
        <ion-buttons slot="start">
          <ion-back-button
            text=""
            mode="ios"
            defaultHref=""
            (click)="backButton()"
          ></ion-back-button>
        </ion-buttons>
      </ion-toolbar>
    </ion-header>
    <ion-content class="ve" *ngIf="this.user$ | async as user">
      <div class="ve__content">
        <div class="ve__content__title">
          <ion-text> Verify that it's you! </ion-text>
        </div>
        <div class="ve__content__description">
          <ion-text>
            Log in to <strong>{{ user?.email }}</strong> and verify that it is
            your account. It may take a while to enter the email, be patient.
            You can also check your SPAM.
          </ion-text>
        </div>
      </div>
    </ion-content>
    <ion-footer class="footer">
      <div class="ve__button__done">
        <app-custom-button
          (buttonClick)="this.done()"
          [buttonName]="'Done'"
        ></app-custom-button>
      </div>
      <div class="ve__button__resend-email">
        <div class="text">
          <ion-text>You did not receive any email?</ion-text>
        </div>
        <app-custom-button
          (buttonClick)="this.resendEmail()"
          [buttonName]="'Resend email'"
        ></app-custom-button>
      </div>
    </ion-footer>`,
  styleUrls: ['./verification-email.page.scss'],
})
export class VerificationEmailPage implements OnInit {
  user$: Observable<any> = this.usersService.auth.user;
  constructor(
    private navController: NavController,
    private usersService: UsersService,
    private toastController: ToastController
  ) {}

  async ngOnInit() {}

  backButton() {
    this.navController.navigateBack('users/login');
  }

  done() {
    this.navController.navigateForward('users/login');
  }

  resendEmail() {
    this.usersService.sendVerificationEmail();
    this.successToast();
  }

  async successToast() {
    const toast = await this.toastController.create({
      message: 'Email successfully forwarded!',
      animated: true,
      duration: 1500,
      position: 'bottom',
      icon: 'checkmark-circle-outline',
      color: 'success',
    });

    await toast.present();
  }
}
