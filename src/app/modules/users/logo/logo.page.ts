import { Component, OnInit } from '@angular/core';
import { FormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { NavController, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular';

@Component({
  selector: 'app-logo',
  template: `<ion-header mode="md">
      <ion-toolbar color="black">
        <ion-buttons slot="start">
          <ion-back-button
            text=""
            mode="ios"
            defaultHref=""
            (click)="backButton()"
          ></ion-back-button>
        </ion-buttons>
      </ion-toolbar>
    </ion-header>
    <ion-content class="content">
      <form [formGroup]="this.form">
        <div class="content__title">
          <ion-text>Change your logo</ion-text>
        </div>
        <div class="content__img">
          <img
            *ngIf="this.imageFullPath"
            [src]="this.imageFullPath"
            alt="logo"
          />
        </div>
        <div class="content__upload-img">
          <ion-item>
            <input
              formControlName="image"
              class="content__upload-img__item input"
              id="upload-image"
              name="upload-image"
              type="file"
              accept="image/jpeg,image/png"
              (change)="onFileSelected($event)"
            />
          </ion-item>
        </div>
      </form>
    </ion-content>
    <ion-footer class="footer">
      <div class="content__button">
        <app-custom-button
          (buttonClick)="this.saveImage()"
          [disabled]="!this.form.valid"
          [buttonName]="'Save'"
        ></app-custom-button>
      </div>
    </ion-footer> `,
  styleUrls: ['./logo.page.scss'],
})
export class LogoPage {
  form: UntypedFormGroup = this.formBuilder.group({
    image: ['', [Validators.required]],
  });
  imageFullPath: string;
  constructor(
    private storage: Storage,
    private navController: NavController,
    private formBuilder: FormBuilder,
    private toastController: ToastController
  ) {}

  ionViewWillEnter() {
    this.getImageFromStorage();
  }

  backButton() {
    this.navController.navigateBack('movies/home');
  }

  onFileSelected(event: any) {
    const file = event.target.files[0];
    if (file) {
      const imagePath = file.name;
      this.imageFullPath = 'assets/img/logos/' + imagePath;
    }
  }

  async getImageFromStorage() {
    this.imageFullPath = await this.storage.get('logo');
  }

  async saveImage() {
    await this.storage.set('logo', this.imageFullPath);
    this.successToast();
  }

  async successToast() {
    const toast = await this.toastController.create({
      message: 'Your logo has been successfully changed!',
      animated: true,
      duration: 2300,
      position: 'bottom',
      icon: 'checkmark-circle-outline',
      color: 'success',
    });

    await toast.present();
  }
}
