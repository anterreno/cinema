import { NgModule } from '@angular/core';
import { LogoPage } from './logo.page';
import { RouterModule, Routes } from '@angular/router';
import { SharedUsersModule } from '../shared-users/shared-users.module';

const routes: Routes = [
  {
    path: '',
    component: LogoPage,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes), SharedUsersModule],
  exports: [RouterModule],
  declarations: [LogoPage],
})
export class LogoPageModule {}
