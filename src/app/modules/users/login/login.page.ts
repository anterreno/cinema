import { Component, OnInit } from '@angular/core';
import { FormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { NavController, ToastController } from '@ionic/angular';
import { UsersService } from '../shared-users/service/users.service';
import { Storage } from '@ionic/storage-angular';

@Component({
  selector: 'app-login',
  template: `<ion-content [scrollY]="false">
      <div class="content">
        <div class="content__form">
          <div class="content__form__img">
            <img [src]="this.image" alt="cinema" />
          </div>
          <form [formGroup]="this.form">
            <div class="content__form__email">
              <app-custom-input
                [controlName]="'email'"
                [placeholder]="'Email'"
              ></app-custom-input>
            </div>
            <div class="content__form__password">
              <app-custom-password-input
                [controlName]="'password'"
                [placeholder]="'Password'"
              >
              </app-custom-password-input>
            </div>
          </form>
        </div>
        <div class="content__button">
          <app-custom-button
            (buttonClick)="this.signIn()"
            [disabled]="!this.form.valid"
            [buttonName]="'Login'"
          ></app-custom-button>
          <div class="content__button__create-account">
            <ion-button fill="clear" mode="ios" (click)="goToRegisterPage()"
              >Create account</ion-button
            >
          </div>
        </div>
      </div>
    </ion-content>
    <ion-footer class="footer">
      <div class="footer__forgot-password">
        <ion-button fill="clear" mode="ios" (click)="this.recoveryPassword()"
          >Forgot password?</ion-button
        >
      </div>
    </ion-footer>`,
  styleUrls: ['./login.page.scss'],
})
export class LoginPage {
  form: UntypedFormGroup = this.formBuilder.group({
    email: ['', [Validators.required, Validators.email]],
    password: ['', [Validators.required, Validators.minLength(8)]],
  });
  image: string;
  constructor(
    private formBuilder: FormBuilder,
    private usersService: UsersService,
    private toastController: ToastController,
    private navController: NavController,
    private storage: Storage
  ) {}

  async ionViewWillEnter() {
    await this.getLogoFromStorage();
  }

  async getLogoFromStorage() {
    this.image = await this.storage.get('logo');
  }

  signIn() {
    this.usersService
      .signIn(this.form.value.email, this.form.value.password)
      .then((res) => {
        if (res && res.user?.emailVerified) {
          this.successToast();
          this.navController.navigateForward('/movies/home');
        } else if (res && !res.user?.emailVerified) {
          this.navController.navigateForward('/users/verification-email');
        }
      })
      .catch(() => this.errorToast());
  }

  async successToast() {
    const toast = await this.toastController.create({
      message: 'You are successfully logged in!',
      animated: true,
      duration: 1500,
      position: 'bottom',
      icon: 'checkmark-circle-outline',
      color: 'success',
    });

    await toast.present();
  }

  async errorToast() {
    const toast = await this.toastController.create({
      message: 'User or password incorrect. Please try again!',
      animated: true,
      duration: 2000,
      position: 'bottom',
      icon: 'information-circle-outline',
      color: 'danger',
    });

    await toast.present();
  }

  goToRegisterPage() {
    this.navController.navigateForward('users/register');
  }

  recoveryPassword() {
    this.navController.navigateForward('users/forgot-password');
  }
}
