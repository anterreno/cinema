import { NgModule } from '@angular/core';
import { LoginPage } from './login.page';
import { RouterModule, Routes } from '@angular/router';
import { SharedUsersModule } from '../shared-users/shared-users.module';

const routes: Routes = [
  {
    path: '',
    component: LoginPage,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes), SharedUsersModule],
  exports: [RouterModule],
  declarations: [LoginPage],
})
export class LoginPageModule {}
