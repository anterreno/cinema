import { Component, OnInit } from '@angular/core';
import { FormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { NavController, ToastController } from '@ionic/angular';
import { UsersService } from '../shared-users/service/users.service';

@Component({
  selector: 'app-register',
  template: ` <ion-header mode="md">
      <ion-toolbar color="black">
        <ion-buttons slot="start">
          <ion-back-button
            text=""
            mode="ios"
            defaultHref=""
            (click)="backButton()"
          ></ion-back-button>
        </ion-buttons>
      </ion-toolbar>
    </ion-header>
    <ion-content class="content" [scrollY]="false">
      <div class="content__title">
        <ion-text>New User</ion-text>
      </div>
      <div class="content__form">
        <form [formGroup]="this.form">
          <div class="content__form__email">
            <app-custom-input
              [controlName]="'email'"
              [placeholder]="'Email'"
            ></app-custom-input>
          </div>
          <div class="content__form__password">
            <app-custom-password-input
              [controlName]="'password'"
              [placeholder]="'Password'"
            >
            </app-custom-password-input>
          </div>
        </form>
      </div>
      <div class="content__button">
        <app-custom-button
          (buttonClick)="this.registerUser()"
          [disabled]="!this.form.valid"
          [buttonName]="'Register'"
        ></app-custom-button>
      </div>
    </ion-content>`,
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage {
  form: UntypedFormGroup = this.formBuilder.group({
    email: ['', [Validators.required, Validators.email]],
    password: ['', [Validators.required, Validators.minLength(8)]],
  });

  constructor(
    private formBuilder: FormBuilder,
    private usersService: UsersService,
    private toastController: ToastController,
    private navController: NavController
  ) {}

  backButton() {
    this.navController.navigateBack('users/login');
  }

  registerUser() {
    this.usersService
      .signUp(this.form.value.email, this.form.value.password)
      .then(async () => {
        await this.successToast();
        this.navController.navigateForward('/users/verification-email');
      })
      .catch((err) => {
        if (err.message.includes('is already in use')) {
          this.errorToast();
        }
      });
  }

  async successToast() {
    const toast = await this.toastController.create({
      message: 'User successfully registered!',
      animated: true,
      duration: 1500,
      position: 'bottom',
      icon: 'checkmark-circle-outline',
      color: 'success',
    });

    await toast.present();
  }

  async errorToast() {
    const toast = await this.toastController.create({
      message:
        'The email address is already in use by another account. Please try again!',
      animated: true,
      duration: 2000,
      position: 'bottom',
      icon: 'information-circle-outline',
      color: 'danger',
    });

    await toast.present();
  }
}
