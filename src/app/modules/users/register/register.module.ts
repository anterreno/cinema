import { NgModule } from '@angular/core';
import { RegisterPage } from './register.page';
import { RouterModule, Routes } from '@angular/router';
import { SharedUsersModule } from '../shared-users/shared-users.module';

const routes: Routes = [
  {
    path: '',
    component: RegisterPage,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes), SharedUsersModule],
  exports: [RouterModule],
  declarations: [RegisterPage],
})
export class RegisterPageModule {}
