import { Component, Input, OnInit } from '@angular/core';
import { ControlContainer, FormGroupDirective } from '@angular/forms';

@Component({
  selector: 'app-custom-password-input',
  template: ` <ion-item fill="outline" mode="md">
    <ion-input
      [placeholder]="this.placeholder"
      [formControlName]="this.controlName"
      [type]="this.type"
      class="custom-input"
    ></ion-input>
    <button class="log__input__item__eyes" (click)="toggle()">
      <ion-icon *ngIf="this.showPassword" name="eye-outline"></ion-icon>
      <ion-icon *ngIf="!this.showPassword" name="eye-off-outline"></ion-icon>
    </button>
  </ion-item>`,
  styleUrls: ['./custom-password-input.component.scss'],
  viewProviders: [
    {
      provide: ControlContainer,
      useExisting: FormGroupDirective,
    },
  ],
})
export class CustomPasswordInputComponent implements OnInit {
  @Input() placeholder: string;
  @Input() controlName: string;
  type = 'password';
  showPassword = false;
  constructor() {}

  ngOnInit() {}

  toggle() {
    if (this.showPassword) {
      this.showPassword = false;
      this.type = 'password';
    } else {
      this.showPassword = true;
      this.type = 'text';
    }
  }
}
