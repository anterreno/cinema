import { Component, OnInit, Output, Input, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-custom-button',
  template: ` <ion-button
    mode="ios"
    [disabled]="this.disabled"
    expand="block"
    (click)="this.click()"
  >
    {{ this.buttonName }}
  </ion-button>`,
  styleUrls: ['./custom-button.component.scss'],
})
export class CustomButtonComponent implements OnInit {
  @Output() buttonClick: EventEmitter<any> = new EventEmitter<any>();
  @Input() disabled: boolean;
  @Input() buttonName: string;
  constructor() {}

  ngOnInit() {}

  click() {
    this.buttonClick.emit();
  }
}
