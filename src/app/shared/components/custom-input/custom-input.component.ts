import { Component, OnInit, Input } from '@angular/core';
import { ControlContainer, FormGroupDirective } from '@angular/forms';

@Component({
  selector: 'app-custom-input',
  template: `
    <ion-item fill="outline" mode="md">
      <ion-input
        [placeholder]="this.placeholder"
        [formControlName]="this.controlName"
        [type]="this.type"
        class="custom-input"
      ></ion-input>
    </ion-item>
  `,
  styleUrls: ['./custom-input.component.scss'],
  viewProviders: [
    {
      provide: ControlContainer,
      useExisting: FormGroupDirective,
    },
  ],
})
export class CustomInputComponent implements OnInit {
  @Input() placeholder: string;
  @Input() controlName: string;
  @Input() type: string;

  constructor() {}

  ngOnInit() {}
}
