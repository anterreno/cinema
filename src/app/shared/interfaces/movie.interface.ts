export interface Movie {
  id: number;
  title: string;
  description: string;
  rate: number;
  img: string;
}
