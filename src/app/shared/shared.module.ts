import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import {
  FormGroup,
  FormGroupDirective,
  ReactiveFormsModule,
} from '@angular/forms';
import { CustomButtonComponent } from '../shared/components/custom-button/custom-button.component';
import { CustomInputComponent } from '../shared/components/custom-input/custom-input.component';
import { MovieComponent } from '../modules/movies/shared/components/movie/movie.component';
import { IonRatingStarsModule } from 'ion-rating-stars';
import { ModalComponent } from '../modules/movies/shared/components/modal/modal.component';
import { CustomPasswordInputComponent } from './components/custom-password-input/custom-password-input.component';

@NgModule({
  declarations: [
    CustomButtonComponent,
    CustomInputComponent,
    MovieComponent,
    ModalComponent,
    CustomPasswordInputComponent,
  ],
  imports: [
    CommonModule,
    IonicModule.forRoot(),
    ReactiveFormsModule,
    IonRatingStarsModule,
  ],
  exports: [
    CommonModule,
    IonicModule,
    ReactiveFormsModule,
    CustomButtonComponent,
    CustomInputComponent,
    MovieComponent,
    ModalComponent,
    CustomPasswordInputComponent,
  ],
})
export class SharedModule {}
