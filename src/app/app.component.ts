import { Component, OnInit } from '@angular/core';
import { MenuController, NavController } from '@ionic/angular';
import { UsersService } from './modules/users/shared-users/service/users.service';
import { Storage } from '@ionic/storage-angular';

@Component({
  selector: 'app-root',
  template: ` <ion-app>
    <ion-menu contentId="main-content">
      <ion-header class="header">
        <ion-toolbar class="header__toolbar" color="black">
          <ion-title>Menú</ion-title>
        </ion-toolbar>
      </ion-header>
      <ion-content class="content ion-padding">
        <div class="content__change-logo">
          <ion-item detail="true" button (click)="this.goToChangeLogo()">
            <ion-icon name="person-circle-outline" slot="start"></ion-icon>
            <ion-label> Profile </ion-label>
          </ion-item>
        </div>
        <div class="content__logout">
          <ion-item (click)="this.logOut()">
            <ion-label> LogOut </ion-label>
            <ion-icon name="log-out-outline" slot="end"></ion-icon>
          </ion-item>
        </div>
      </ion-content>
    </ion-menu>
    <ion-router-outlet id="main-content"></ion-router-outlet>
  </ion-app>`,
  styleUrls: ['app.component.scss'],
})
export class AppComponent implements OnInit {
  constructor(
    private usersService: UsersService,
    private navController: NavController,
    private menuController: MenuController,
    private storage: Storage
  ) {}

  ngOnInit(): void {
    this.storage.create();
    this.setStorageLogo();
  }

  logOut() {
    this.usersService.signOut().then(() => {
      this.menuController.close();
      this.navController.navigateBack('users/login');
    });
  }

  setStorageLogo() {
    this.storage.set('logo', 'assets/img/logos/cinema.png');
  }

  goToChangeLogo() {
    this.menuController.close();
    this.navController.navigateForward('users/logo');
  }
}
